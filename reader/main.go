package main

import (
	"archive/zip"
	"bytes"
	"fmt"
	"os"
)

func main() {
	printZip("/Users/nico/workspace/tmp/test-dc/a.zip")
	fmt.Println("=======")
	printZip("/Users/nico/workspace/nico/html2docx/test/test.zip")
}

func printZip(path string) {
	bs, err := os.ReadFile(path)
	if err != nil {
		panic(err)
	}

	r, err := zip.NewReader(bytes.NewReader(bs), int64(len(bs)))
	if err != nil {
		panic(err)
	}

	for _, f := range r.File {
		fmt.Println(f.Name, f.CompressedSize64, f.UncompressedSize64, f.Mode())
	}
}
