package main

import (
	"dc/parser"
	_ "embed"
	"fmt"
	"os"
)

var (
	//go:embed test1.html
	bytes []byte
)

func main() {
	p, err := parser.New(bytes)
	if err != nil {
		panic(err)
	}
	err = p.Parse(map[string]interface{}{
		"code":                 "仓库1",
		"dateOfWarehouseEntry": "2023-01-01 11:11:11",
		"totalAmount":          100.1,
		"remark":               "this is a remark",
		"ratio":                0.13,
		"items": []interface{}{
			map[string]interface{}{
				"spuCode": "spu001",
				"skuCode": "spu001001",
				"color":   "red",
				"size":    "XL",
				"count":   104,
				"price":   10.1,
				"desc":    "test1",
			},
			map[string]interface{}{
				"spuCode": "spu002",
				"skuCode": "spu001001",
				"color":   "red",
				"size":    "XL",
				"count":   103,
				"price":   10.1,
				"desc":    "test1",
			},
			map[string]interface{}{
				"spuCode": "spu001",
				"skuCode": "spu001001",
				"color":   "red",
				"size":    "X",
				"count":   102,
				"price":   10.1,
				"desc":    "test1",
			},
			map[string]interface{}{
				"spuCode": "spu002",
				"skuCode": "spu001001",
				"color":   "red",
				"size":    "X",
				"count":   101,
				"price":   10.1,
				"desc":    "test1",
			},
		},
	})
	if err != nil {
		panic(err)
	}
	fmt.Println(p.String())
	err = os.WriteFile("/Users/nico/workspace/treelab/treelab/treelab-doc-creator-poc/sample.html", []byte(p.String()), 0766)
	if err != nil {
		panic(err)
	}
}
