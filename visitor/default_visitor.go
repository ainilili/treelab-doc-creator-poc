package visitor

import (
	"dc/context"
	"golang.org/x/net/html"
)

type defaultVisitor struct{}

func (t *defaultVisitor) Visit(ctx *context.Context, node *html.Node) error {
	if node.Type == html.TextNode {
		node.Data = ctx.Parse(node.Data)
	}
	return nil
}
