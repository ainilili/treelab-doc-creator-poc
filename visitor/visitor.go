package visitor

import (
	"dc/context"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

const (
	TData               = "t-data"
	TRange              = "t-range"
	TStart              = "t-start"
	TLoop               = "t-loop"
	TVar                = "t-var"
	TTransposeDimension = "t-transpose-dimension"
	TTransposeValue     = "t-transpose-value"
	THeader             = "t-header"

	TransposeMark = "dimension_"
)

var visitors = map[atom.Atom]Visitor{
	atom.Table: &tableVisitor{},
}

var DefaultVisitor = &defaultVisitor{}

type Visitor interface {
	Visit(ctx *context.Context, node *html.Node) error
}

func GetVisitor(a atom.Atom) (Visitor, bool) {
	v, ok := visitors[a]
	return v, ok
}
