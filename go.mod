module dc

go 1.19

require (
	github.com/ericchiang/pup v0.4.0
	github.com/gammazero/deque v0.2.1
	github.com/larstos/jsonpath_go v0.0.0-20210608084149-32a084235e69
	github.com/unidoc/unioffice v1.22.0
	github.com/valyala/fasttemplate v1.2.2
	golang.org/x/net v0.0.0-20220722155237-a158d28d115b
)

require (
	github.com/fatih/color v1.15.0 // indirect
	github.com/larstos/convert v0.0.0-20210409072724-dc1ef6f10686 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/richardlehane/msoleps v1.0.1 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.4.0 // indirect
)
